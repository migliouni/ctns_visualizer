# ctns-visualizer

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)


COVID simulator and network evolution introduce a tool architecture of out networked SEIR model, keeping separate  the simulation process from the visualization. 
The simulator allows us to start theSARS-CoV-2 contact simulator, which has different initial parameters that it is possible to customize. 
Simulations are created with [ctns (Contact Tracing Network Simulator)](https://gitlab.com/mistrello96/ctns), a tool to simulate digital contact networks beetween people in a population where a disease is spreading

### Implementation
The entire application has been developed using python. In particular, the main libraries used are: igraph and dash.
The application has the following structure:
```
.                                                  
├ network_dumps
├ network_dumps_examples
├ src
    |-app.py
    |-callbacks.py
    |-layouts.py
├ create_images.py
├ net_evolution_app.py
├ .gitignore
├ requirements.txt
└ README.md
```
it is important to distinguish the two folders: *network_dumps* and *network_dumps_examples*. The first contains all the pickle networks that you want to visualize. 
It is essential that all the pickles files (type full dump) are present in this folder. The second folder contains some examples that can always be used for testing.
After copying the networks into the *network_dumps* folder, you need to run the *create_images.py* script to create GIF and simulation images. 
The python code: *create_images.py* read all simulation file (pickle full dump) in the folder *network_dumps* and create simulation images and GIF. 
After creating them, it saves GIF in "assets" folder while daily images in "images" folder. In particular:
* It creates a graph with all infected node and edges that could transmit the infection.
* It creates the image of the Igraph in a specific day.
* It creates the image with infected node of the graph in a specific day.

vice versa, *net_evolution_app.py* represents the visualizator which will be discussed later.

### Installation
the use of the application, in the online demo version does not require any installation. In this case, the interface is immediately availble into the horoku server: [visualizer](https://mind-lab-networked-covid-vis.herokuapp.com/). 
Attention: for large analysis, it is recommended to use the local version, because the online version, is a light version only for demo and it may present limitation. 
Moreover in the demo version is possible to view only example presented in *network_dumps_examples*.

You can also download the application for local use. In this case, simply run the following command:
```sh
$ git clone https://gitlab.com/migliouni/ctns_visualizer.git
```
If you want run locally the application, you need to install some tools.
The first libraries is [igraph](https://igraph.org/python/#docs). It is a collection of libraries for graph creation, manipulation and network analysis.
```sh
$ pip install python-igraph
```
For the visualization part it is necessary to install [plotly dash](https://pypi.org/project/dash/)
```sh
$ pip install dash
```
For the visualization are necessary two other libraries: *matplotlib* and *pillow*
```sh
$ pip install matplotlib
$ pip install pillow
```

More easily, you can run the following command to install all the libraries required by the application
```sh
pip install -r requirements.txt
```
After installing all the libraries, put all the simulations to be displayed in the "network dumps" folder. 
Then you must launch "create_images.py" code it through the command. 
```sh
$ python create_images.py
```
"create_images.py" python code takes all the files in the "network dumps" folder and creates the images and the corresponding GIF for each one. Saving in the images and assets folders
Through a progress bar is shown the percentage of completion of the work.
```sh
Processing file: "network_dumps\sim_dump0.pickle"
 23%|██████████████████▉                                                              | 35/150 [00:13<01:06,  1.72it/s]
```
 When all images are created and save in images folder then you can run the program with the following command
```sh
$ python net_evolution_app.py
```
At the end of the launch, it is possible to see the following output
```sh
Running on http://127.0.0.1:8888/
Debugger PIN: 867-011-087
 * Serving Flask app "net_evolution_app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
```
At this point, you must copy the link above and past it into a browser.

It is recommended the last version of python 3.x [click here for the download](https://www.python.org/downloads/).


## Visualization
The visualization part is fully available through the user interface and very intuitive to use. We distinguish two parts: **network evolution** which aims to provide a day-to-day representation of the infected network. It allows to visualize all the most significant information and allows you to understand what happens in the evolution of time. In addition, the second part is called **Simulations comparison** which has the purpose to compare different networks obtained by the simulation process, and it provides  useful tools in order to highlight differences according to the parameters that have been established.

### Different types of nodes
Nodes in the network fall into one of five exclusive states:
-   Susceptible, nodes that are not yet infected
-   Exposed, represents the people that have been infected, but they are not contagious yet
-   Infected, nodes that can trasmit the disease to a susceptible individual
-   Recovered, people recovered from the disease, these individuals are immune to the infection
-   Dead, nodes not survivor at the disease

We assumed that recoverd individuals do not become susceptible, but enjoy permanent immunity. The total population size was fixed.

### Network evolution
we carefully analyze the different graphs available.

#####  Network evolution
This part allows us to choose the network that we want to analyze. after selecting it, we can visualize the simulation summary. Throught a dynamic representation, we can observe the evolution of the network day-by-day. Each type of node is clearly distinguished according to the description given above. In particular, the blue color is associated with Susceptible nodes, orange color is associated with Exposed nodes, red color is associated with Infected nodes, green color is associated with Recovered nodes and black color is associated with Dead nodes.
<img alt="Esample Network evolution" title="Network evolution1" src="https://imgur.com/PrZWsj2.gif" width="300">
<img alt="Example Network evolution" title="Network evolution 2" src="https://imgur.com/18nyB4P.gif" width="300">



#####  Evolution day by day
The previous representation allows us to visualize evolution of the network day by day but it doesn't allow us to set or stop our attention in a certain day. This component provides slidebar and buttons with the purpose of going back and forth manually on the days. 
Here, it is possibile to visualize two different plots:
* The visualization of the network graph with the classic representation
* The visualization of the network graph with a grid layout representation
<img alt="Example Evolution day by day" title="Evolution day by day" src="https://imgur.com/YvxS1hD.png" width="500">


#####  Before and current
These plots change dynamically according to the day defined on the previous slidebar. In particular, this tab shows the differences between the current day setted on the slidebar, with the previous day. This tab provide us a tool to better understand the short term evolution of the network.
<img alt="Example Before and current" title="Before and current" src="https://imgur.com/H8nC2Ki.png" width="600">

#####  Evolution day by day
This component completes the visualization day by day provides previously. Here, you can see the evolution of the net dynamically. You can choose the day throught the slidebar and visualize the relative plot of the situation. You can see also the relative graph. The aim of the lineplot is to show the long term evolution of the network in terms of SEIRD classes.
Here, it is possibile to visualize two different plots:
* A dynamic line plot where it is possible to follow the quantitative evolution of the network
* A static PNG image that shows the networtk status in a selected day.
<img alt="Example Evolution day by day" title="Evolution day by day" src="https://imgur.com/BXSBAB8
.png" width="600">

### Simulations comparison
This parts is different to the previous one. While the Network evolution section had the purpose of analyzing a single network, this section has the goal of comparing different networks in order to determine how these change as the parameters vary.

Initially, the network dumps in .pickle format must be uploaded into the tools. At the end of the upload it will be possible to display different graphs.
Now, we analyse all components of this part:

#### Simulation parameters
It is a table in which are summarized the main parameters involved in the simulation.

#### Restriction parameters
It is a table in which are summarized the restrition values defined in the simulation.

#### Testing and quaratine parameters
It is a table in which are summarized the tests carried out and the quarantine parameters

these three tables are displayed in the following figure:

<img alt="Example Simulation parameters" title="Simulation parameters" src="https://imgur.com/ZYJsBV0.png" width="600">

#### Infected day by day
It is a heatmap which highlights the days when there were the highest number of infected.

<img alt="Example Infected day by day" title="Infected day by day" src="https://imgur.com/dvYB9zu.png" width="600">

#### Comparison component
In this area, it is possible to visualize five different plots:
* Comparison infected among the different simulations, it is a lineplot
* Comparison dead among the differnt siulations, it is a lineplot
* Comparison total infected at the end of simulation, it is a barplot
* Comparison length simulation in terms of days, it is a barplot
* Comparison dead  among the different simulations
<img alt="Example Comparison component" title="Comparison component" src="https://imgur.com/ARlmHhO.png" width="600">
<img alt="Example Comparison component" title="Comparison component" src="https://imgur.com/64xuUKC.png" width="600">

#### Summary at the end of simulations
This barplot summarize the SEIRD variables at the end of the simulations.

<img alt="Example Simulation parameters" title="Simulation parameters" src="https://imgur.com/UryTZvx.png" width="600">
import igraph as ig
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import glob, pickle, os
import numpy as np
from pathlib import Path
from matplotlib.collections import LineCollection
from PIL import Image
from pathlib import Path
from collections import Counter
from tqdm import tqdm



def images_to_gif(name, images):
    """
    Read all simulation images and create GIF. Save the results in "assets" folder
    
    Parameters
    ----------
    name: string
        The name of the result GIF and PNG file

    images: string
        The list of images names
    Return
    ------
    None
    Save the results on file

    """
    imagesList = []
    for image in images:
        imagesList.append(Image.open(image))


    imagesList[0].save(Path('assets/{}_simulation.png'.format(name)))
    imagesList[0].save(Path('assets/{}_simulation.gif'.format(name)), save_all=True, append_images=imagesList[1:], optimize=True, duration=400, loop=0)



def create_graph__infected_history(nets, layout, file_name):
    """
    Create a graph with all infected node and edges that could transmit the infection
    
    Parameters
    ----------
    nets: list of igraph object (graph)
        All the networks of the simulation 
    
    layout: list of tuple
        Couple of (x, y) position of the node for plotting

    file_name: string
        The name of the result image
    
    Return
    ------
    None
    Save the results on file (pdf format)

    """
    node_size = 65
    node_shape = 'o'
    
    image_nodes = set()
    image_edges = set()

    for day in range(len(nets)):
        # curretn network
        G = nets[day]
        if day > 0:
            yesterday_infected =  set([x.index for x in nets[day - 1].vs() if x['agent_status'] == 'I'])
            yesterday_exposed =  set([x.index for x in nets[day - 1].vs() if x['agent_status'] == 'E'])
        else:
            yesterday_infected = set()
            yesterday_exposed = set()

    
    
    
        infected = set([vertex.index for vertex in G.vs if vertex["agent_status"] == 'I'])
        exposed = set([vertex.index for vertex in G.vs if vertex["agent_status"] == 'E'])

        
        new_infected = infected - yesterday_infected
        new_exposed = exposed - yesterday_exposed

        image_nodes = image_nodes | new_exposed

        for edge in G.es:
            if (edge.source in new_exposed and edge.target in infected) or (edge.target in new_exposed and edge.source in infected):
                image_edges.add(edge)            
    

       
    
    xy = np.asarray([layout[v] for v in image_nodes])

        
    edge_pos = np.asarray([(layout[e.source], layout[e.target]) for e in image_edges])
                
                
    fig, ax = plt.subplots(figsize=(5, 5))
    fig.patch.set_visible(False)
    ax.axis('off')
    node_collection = ax.scatter(xy[:, 0], xy[:, 1],
                                     s=node_size,
                                     c = "blue"
                                     )
            

    ax.tick_params(axis='both',
                   which='both',
                   bottom=False,
                   left=False,
                   labelbottom=False,
                   labelleft=False)

    node_collection.set_zorder(2)

    edge_collection = LineCollection(edge_pos,
                                     linewidths = 0.35,
                                     linestyle = 'solid',
                                     colors = 'gray'
                                    )
    

    edge_collection.set_zorder(1)  # edges go behind nodes
    ax.add_collection(edge_collection)

    ax.set_title('Contagion network')
                
    #save fig
    #plt.savefig(, dpi = 100, optimize = True)
    
    plt.savefig(Path('assets/{}_contagion_network.pdf'.format(file_name)), dpi = 100, optimize = True)
    
    #plt.show()
    plt.cla()
    plt.clf()
    plt.close('all')




def create_dayly_image(nets, day, layout, file_name, save_pdf = False):
    """
    Create the image of the graph in a specific day. The position of the nodes is fixed according to the layout argument. Save the results in "images" folder
    
    Parameters
    ----------
    nets: list of igraph object (graph)
        All the networks of the simulation 
    day: integer
        Current day of simulation

    layout: list of tuple
        Couple of (x, y) position of the node for plotting

    file_name: string
        The name of the result image
    
    save_pdf: bool
        Save current image in pdf format (default = False)
    
    Return
    ------
    None
    Save the results on file (jpeg format)

    """
               
    # plot option
    colors = {'S':'#0000ff', 'E':'#ffa300', 'I':'#ff0000', 'D':'#000000', 'R':'#00ff00'}
    node_size = 65
    node_shape = 'o'
    
    # curretn network
    G = nets[day]
    vertex_color = [colors[status] for status in G.vs["agent_status"]]
    edge_color = []

    infected = [vertex.index for vertex in G.vs if vertex["agent_status"] == 'I']
            
    for edge in G.es:
        if edge.source in infected or edge.target in infected:
            edge_color.append('red')
        else:
            edge_color.append('lightgrey')
            

    xy = np.asarray([layout[v] for v in range(len(G.vs))])
            
    edge_pos = np.asarray([(layout[e.source], layout[e.target]) for e in G.es])
            
            
    fig, ax = plt.subplots(figsize=(5, 5))
    fig.patch.set_visible(False)
    ax.axis('off')
    node_collection = ax.scatter(xy[:, 0], xy[:, 1],
                                     s=node_size,
                                     c = vertex_color
                                     )
            

    ax.tick_params(axis='both',
                   which='both',
                   bottom=False,
                   left=False,
                   labelbottom=False,
                   labelleft=False)

    node_collection.set_zorder(2)

    edge_collection = LineCollection(edge_pos,
                                     colors = edge_color,
                                     linewidths=0.35,
                                     linestyle='solid',
                                    )

    edge_collection.set_zorder(1)  # edges go behind nodes
    ax.add_collection(edge_collection)

    ax.set_title('Day: ' + str(day))


    patch1 = mpatches.Patch(color=colors['S'], label='S')
    patch2 = mpatches.Patch(color=colors['E'], label='E')
    patch3 = mpatches.Patch(color=colors['I'], label='I')
    patch4 = mpatches.Patch(color=colors['R'], label='R')
    patch5 = mpatches.Patch(color=colors['D'], label='D')

    ax.legend(handles=[patch1, patch2, patch3, patch4, patch5], ncol=5, loc='lower center')
    plt.tight_layout()
                

    #save fig
    plt.savefig(file_name, dpi = 100, optimize = True)
    if save_pdf == True:
        plt.savefig(file_name.split(".")[0] + str('pdf'), dpi = 100, optimize = True)
    plt.cla()
    plt.clf()
    plt.close('all')




"""
    Create the image with infected node of the graph in a specific day. The position of the nodes is fixed according to the layout argument. Save the results in "images" folder
    
    Parameters
    ----------
    nets: list of igraph object (graph)
        All the networks of the simulation 
    day: integer
        Current day of simulation

    layout: list of tuple
        Couple of (x, y) position of the node for plotting

    file_name: string
        The name of the result image
    
    save_pdf: bool
        Save current image in pdf format (default = False)
    
    Return
    ------
    None
    Save the results on file (jpeg format)

    """
def create_dayly_image_infected(nets, day, layout, file_name, save_pdf = False):
    # plot option
    colors = {'S':'#0000ff', 'E':'#ffa300', 'I':'#ff0000', 'D':'#000000', 'R':'#00ff00'}
    node_size = 65
    node_shape = 'o'
    
    # curretn network
    G = nets[day]
    if day > 0:
        yesterday_infected =  set([x.index for x in nets[day - 1].vs() if x['agent_status'] == 'I'])
        yesterday_exposed =  set([x.index for x in nets[day - 1].vs() if x['agent_status'] == 'E'])
    else:
        yesterday_infected = set()
        yesterday_exposed = set()

    
    
    edge_color = []
    
    infected = set([vertex.index for vertex in G.vs if vertex["agent_status"] == 'I'])
    exposed = set([vertex.index for vertex in G.vs if vertex["agent_status"] == 'E'])

    vertex_color = [colors[G.vs[index]['agent_status']] for index in infected | exposed]
    
    new_infected = infected - yesterday_infected
    new_exposed = exposed - yesterday_exposed


    edge_inf = list()
    for edge in G.es:
        if (edge.source in new_exposed and edge.target in infected) or (edge.target in new_exposed and edge.source in infected):
            edge_color.append('red')
            edge_inf.append(edge)
        #else:
        #    edge_color.append('lightgrey')
    
       
    if len(infected | exposed) > 0:
        xy = np.asarray([layout[v] for v in infected | exposed])

        
        edge_pos = np.asarray([(layout[e.source], layout[e.target]) for e in edge_inf])
                
                
        fig, ax = plt.subplots(figsize=(5, 5))
        fig.patch.set_visible(False)
        ax.axis('off')
        node_collection = ax.scatter(xy[:, 0], xy[:, 1],
                                         s=node_size,
                                         c = vertex_color
                                         )
                

        ax.tick_params(axis='both',
                       which='both',
                       bottom=False,
                       left=False,
                       labelbottom=False,
                       labelleft=False)

        node_collection.set_zorder(2)

        edge_collection = LineCollection(edge_pos,
                                         colors = edge_color,
                                         linewidths=0.35,
                                         linestyle='solid',
                                        )
        
        xy_blank = np.asarray([layout[v.index] for v in G.vs()])
        blank_node = ax.scatter(xy_blank [:, 0], xy_blank [:, 1],
                                         s=node_size,
                                         c = "white"
                                         )
        blank_node.set_zorder(0)

        edge_collection.set_zorder(1)  # edges go behind nodes
        ax.add_collection(edge_collection)

        ax.set_title('Day: ' + str(day))
                    
        #save fig
        plt.savefig(file_name, dpi = 100, optimize = True)
        if save_pdf == True:
            plt.savefig(file_name.split(".")[0] + str('pdf'), dpi = 100, optimize = True)
        #plt.show()
        plt.cla()
        plt.clf()
        plt.close('all')




# read all pickle file and create images, gif and pdf
if __name__ == "__main__":
    
    # create dir fo result if not exist
    if not os.path.exists("images"):
        os.mkdir("images")

    if not os.path.exists("assets"):
        os.mkdir("assets")
    
    # remove old images and old files
    toRemove = glob.glob("images/*.jpeg")
    
    toRemove.extend(glob.glob("assets/*.gif"))
    
    toRemove.extend(glob.glob("assets/*.png"))
    
    toRemove.extend(glob.glob("assets/*.pdf"))
   
    toRemove.extend(glob.glob("assets/*.pickle"))
    
    for file in toRemove:
        os.remove(file)
    
    # get dump file
    files = os.listdir('network_dumps/')
    all_pickles_names = [i for i in files if i.endswith('.pickle')]

    # file to process
    for current_pickle_name in all_pickles_names:

        # Status of evry node day by day
        network_history = {}
        network_history['history'] = {}
        nets = list()
        simulation_names = []
        simulation_infected_names = []
        
        # current dump to processing
        current_file_path = Path("network_dumps/{}".format(current_pickle_name))

        print("\nProcessing file:", current_file_path)
        with open(current_file_path, "rb") as f:
            dump = pickle.load(f)
            # check full dump
            try:
                nets = dump['nets']
            except:
                print("Can not create images without full dump!")
                continue
        
        # the total number of simulation days
        tot = len(nets)
            
        name_daily_simulation = current_pickle_name.split(".")[0]
        name_daily_simulation_only_infected = current_pickle_name.split(".")[0] + str("_inf")
        graph_history_name = current_pickle_name.split(".")[0]

        path_images = str(Path("images/{}_sim".format(name_daily_simulation)))

        # load first network and calculate the position of vertices 
        G = nets[0]

        #fix the vertices position
        layout = G.layout("large")

        list_count_positive = list()
        
        list_prob_avg = list()

        create_graph__infected_history(nets, layout, graph_history_name)
        # tqdm use for progress bar
        with tqdm(total = tot) as pbar:
            for day in range(0, tot):
                network_history['history'][day] = Counter(nets[day].vs["agent_status"])

                # check simulation end
                if network_history['history'][day]['I'] + network_history['history'][day]['E'] == 0:
                    del network_history['history'][day]
                    # update simulation progress bar at 100% value
                    pbar.update(tot - day)
                    break
                
                
                # take count of test result
                #count_positive =  sum([value for value in nets[day].vs['test_result'] if value == 1])

                count_positive = 0
                
                for node in nets[day].vs:
                    tested = None
                    test_result = None
                    if day > 0 and node['test_validity'] > 0 and nets[day -1].vs[node.index]['test_validity'] == 0:
                        tested = True
                        test_result = node['test_result']
                        
                    # step 0
                    elif day == 0 and node['test_validity'] > 0:
                        tested = True
                        test_result = node['test_result']
                    
                    if tested is not None and test_result == 1:
                        count_positive +=1 


                list_count_positive.append(count_positive)
                
                if 'prob_inf' in nets[day].vs.attributes():
                    prob_avg = np.mean(nets[day].vs['prob_inf'])
                    list_prob_avg.append(prob_avg)
                
                
                sim_daily_name = Path(path_images + str(day) + ".jpeg")

                sim_daily_inf_name = Path(path_images + str(day) + "_inf.jpeg")
                
                create_dayly_image(nets, day, layout, sim_daily_name, save_pdf = False)
                create_dayly_image_infected(nets, day, layout, sim_daily_inf_name, save_pdf = False)


                # add images for the GIF
                simulation_names.append(sim_daily_name)
                
                # add images for infected GIF
                simulation_infected_names.append(sim_daily_inf_name)
                
                # update simulation progress bar
                pbar.update(1)
                

            # create GIF
            images_to_gif(name_daily_simulation, simulation_names)

            # create GIF with only infected nodes
            images_to_gif(name_daily_simulation_only_infected, simulation_infected_names)

            # delete images not useful (infected day by day)
            for image in simulation_infected_names:
                os.remove(image)
            
            
            network_history['layout'] = layout
            network_history['found_positive'] = list_count_positive
            if 'prob_inf' in G.vs.attributes():
                network_history['avg_prob_inf'] = list_prob_avg
            

            #print("network_history: ", network_history)
            
            
            # save network historys
            with open(Path('assets/network_history_{}.pickle'.format(name_daily_simulation)), 'wb') as handle:
                pickle.dump(network_history, handle)
        

        